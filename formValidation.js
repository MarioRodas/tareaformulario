function validarFormulario() {
    var nombre = document.forms["formularioInscripcion"]["nombre"].value;
    var fechaNacimiento = document.forms["formularioInscripcion"]["fechaNacimiento"].value;
    var direccion = document.forms["formularioInscripcion"]["direccion"].value;
    var telefono = document.forms["formularioInscripcion"]["telefono"].value;
    var sexo = document.forms["formularioInscripcion"]["sexo"].value;
    var tarjeta = document.forms["formularioInscripcion"]["tarjeta"].value;
    var vencimiento = document.forms["formularioInscripcion"]["vencimiento"].value;
    var cvv = document.forms["formularioInscripcion"]["cvv"].value;

    if (nombre == "" || fechaNacimiento == "" || direccion == "" || telefono == "" || sexo == "" || tarjeta == "" || vencimiento == "" || cvv == "") {
        alert("Todos los campos deben ser llenados correctamente");
        return false;
    } else {
        return true;
    }
}
